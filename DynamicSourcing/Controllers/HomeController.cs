﻿using DynamicSourcing.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace DynamicSourcing.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private IConfiguration configuration;

        public HomeController(ILogger<HomeController> logger, IConfiguration configuration)
        {
            _logger = logger;
            this.configuration = configuration;
        }

        public IActionResult Index()
        {
            return View();
        }



        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        public IActionResult FillOriginalAssignedRorg(string loadTimeId)
        {
            List<String> originalAssignedRorgList = new List<String>();
            originalAssignedRorgList.Add("");
            string conString = configuration.GetConnectionString("strConnectionString");
            var con = new SqlConnection(conString);
            using (SqlCommand cmd = new SqlCommand("get_OriginalAssignedRorg"))
            {
                con.Open();

                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                if (String.IsNullOrWhiteSpace(loadTimeId))
                    cmd.Parameters.AddWithValue("@LOAD_TIME", DBNull.Value);
                else
                    cmd.Parameters.AddWithValue("@LOAD_TIME", loadTimeId);
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        string OriginalAssignedRorgText = sdr["ORIGINAL_ASSIGNED_RORG"].ToString();
                        originalAssignedRorgList.Add(OriginalAssignedRorgText);
                    }
                }
                con.Close();
            }
            return Ok(originalAssignedRorgList);
        }
        public IActionResult FillOriginalSupplyChainRegion(string loadTimeId)
        {
            List<String> originalSupplyChainRegionList = new List<String>();
            originalSupplyChainRegionList.Add("");
            string conString = configuration.GetConnectionString("strConnectionString");
            var con = new SqlConnection(conString);
            using (SqlCommand cmd = new SqlCommand("get_OriginalSupplyChainRegion"))
            {
                con.Open();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                if (String.IsNullOrWhiteSpace(loadTimeId))
                    cmd.Parameters.AddWithValue("@LOAD_TIME", DBNull.Value);
                else
                    cmd.Parameters.AddWithValue("@LOAD_TIME", loadTimeId);
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        string OriginalSupplyChainRegionText = sdr["ORIGINAL_SUPPLY_CHAIN_REGION"].ToString();
                        originalSupplyChainRegionList.Add(OriginalSupplyChainRegionText);
                    }
                }
                con.Close();
            }
            return Ok(originalSupplyChainRegionList);
        }
        public IActionResult FillFinalAssingedRorg(string loadTimeId)
        {
            List<String> finalAssingedRorgList = new List<String>();
            finalAssingedRorgList.Add("");
            string conString = configuration.GetConnectionString("strConnectionString");
            var con = new SqlConnection(conString);
            using (SqlCommand cmd = new SqlCommand("get_FinalAssingedRorg"))
            {
                con.Open();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                if (String.IsNullOrWhiteSpace(loadTimeId))
                    cmd.Parameters.AddWithValue("@LOAD_TIME", DBNull.Value);
                else
                    cmd.Parameters.AddWithValue("@LOAD_TIME", loadTimeId);
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        string FinalAssingedRorgText = sdr["FINAL_ASSIGNED_RORG"].ToString();
                        finalAssingedRorgList.Add(FinalAssingedRorgText);
                    }
                }
                con.Close();
            }
            return Ok(finalAssingedRorgList);
        }
        public IActionResult FinalSupplyChainRegion(string loadTimeId)
        {
            List<String> finalSupplyChainRegionList = new List<String>();
            finalSupplyChainRegionList.Add("");
            string conString = configuration.GetConnectionString("strConnectionString");
            var con = new SqlConnection(conString);
            using (SqlCommand cmd = new SqlCommand("get_FinalSupplyChainRegion"))
            {
                con.Open();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                if (String.IsNullOrWhiteSpace(loadTimeId))
                    cmd.Parameters.AddWithValue("@LOAD_TIME", DBNull.Value);
                else
                    cmd.Parameters.AddWithValue("@LOAD_TIME", loadTimeId);
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        string FinalSupplyChainRegionText = sdr["FINAL_SUPPLY_CHAIN_REGION"].ToString();
                        finalSupplyChainRegionList.Add(FinalSupplyChainRegionText);
                    }
                }
                con.Close();
            }
            return Ok(finalSupplyChainRegionList);
        }
        public IActionResult Category(string loadTimeId)
        {
            List<String> categoryList = new List<String>();
            categoryList.Add("");
            string conString = configuration.GetConnectionString("strConnectionString");
            var con = new SqlConnection(conString);
            using (SqlCommand cmd = new SqlCommand("get_Category"))
            {
                con.Open();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                if (String.IsNullOrWhiteSpace(loadTimeId))
                    cmd.Parameters.AddWithValue("@LOAD_TIME", DBNull.Value);
                else
                    cmd.Parameters.AddWithValue("@LOAD_TIME", loadTimeId);
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        string categoryText = sdr["CATEGORY"].ToString();
                        categoryList.Add(categoryText);
                    }
                }
                con.Close();
            }
            return Ok(categoryList);
        }
        public IActionResult InterIntraRegion(string loadTimeId)
        {
            List<String> interIntraRegionList = new List<String>();
            interIntraRegionList.Add("");
            string conString = configuration.GetConnectionString("strConnectionString");
            var con = new SqlConnection(conString);
            using (SqlCommand cmd = new SqlCommand("get_InterIntraRegion"))
            {
                con.Open();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                if (String.IsNullOrWhiteSpace(loadTimeId))
                    cmd.Parameters.AddWithValue("@LOAD_TIME", DBNull.Value);
                else
                    cmd.Parameters.AddWithValue("@LOAD_TIME", loadTimeId);
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        string categoryText = sdr["INTER/INTRA REGION"].ToString();
                        interIntraRegionList.Add(categoryText);
                    }
                }
                con.Close();
            }
            return Ok(interIntraRegionList);
        }
        public IActionResult Customer(string loadTimeId)
        {
            List<String> customerList = new List<String>();
            customerList.Add("");
            string conString = configuration.GetConnectionString("strConnectionString");
            var con = new SqlConnection(conString);
            using (SqlCommand cmd = new SqlCommand("get_Customer"))
            {
                con.Open();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                if (String.IsNullOrWhiteSpace(loadTimeId))
                    cmd.Parameters.AddWithValue("@LOAD_TIME", DBNull.Value);
                else
                    cmd.Parameters.AddWithValue("@LOAD_TIME", loadTimeId);
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        string categoryText = sdr["CUSTOMER"].ToString();
                        customerList.Add(categoryText);
                    }
                }
                con.Close();
            }
            return Ok(customerList);
        }
        public IActionResult ScheduleFlag(string loadTimeId)
        {
            List<String> scheduleFlagList = new List<String>();
            scheduleFlagList.Add("");
            string conString = configuration.GetConnectionString("strConnectionString");
            var con = new SqlConnection(conString);
            using (SqlCommand cmd = new SqlCommand("get_ScheduleFlag"))
            {
                con.Open();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                if (String.IsNullOrWhiteSpace(loadTimeId))
                    cmd.Parameters.AddWithValue("@LOAD_TIME", DBNull.Value);
                else
                    cmd.Parameters.AddWithValue("@LOAD_TIME", loadTimeId);
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        scheduleFlagList.Add(sdr["SCHEDULE_FLAG"].ToString());
                    }
                }
                con.Close();
            }
            return Ok(scheduleFlagList);
        }
        public IActionResult SpandAllowanceFlag(string loadTimeId)
        {
            List<String> spandAllowanceFlagList = new List<String>();
            spandAllowanceFlagList.Add("");
            string conString = configuration.GetConnectionString("strConnectionString");
            var con = new SqlConnection(conString);
            using (SqlCommand cmd = new SqlCommand("get_SpendAllowanceFlag"))
            {
                con.Open();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                if (String.IsNullOrWhiteSpace(loadTimeId))
                    cmd.Parameters.AddWithValue("@LOAD_TIME", DBNull.Value);
                else
                    cmd.Parameters.AddWithValue("@LOAD_TIME", loadTimeId);
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        string categoryText = sdr["SPEND_ALLOWANCE_FLAG"].ToString();
                        spandAllowanceFlagList.Add(categoryText);
                    }
                }
                con.Close();
            }
            return Ok(spandAllowanceFlagList);
        }
        public IActionResult MetFlag(string loadTimeId)
        {
            List<String> metFlagList = new List<String>();
            metFlagList.Add("");
            string conString = configuration.GetConnectionString("strConnectionString");
            var con = new SqlConnection(conString);
            using (SqlCommand cmd = new SqlCommand("get_MetFlag"))
            {
                con.Open();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                if (String.IsNullOrWhiteSpace(loadTimeId))
                    cmd.Parameters.AddWithValue("@LOAD_TIME", DBNull.Value);
                else
                    cmd.Parameters.AddWithValue("@LOAD_TIME", loadTimeId);
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        metFlagList.Add(sdr["MET_FLAG"].ToString());
                    }
                }
                con.Close();
            }
            return Ok(metFlagList);
        }
        public IActionResult LoadTime()
        {
            List<DateTime> loadTimeList = new List<DateTime>();
            string conString = configuration.GetConnectionString("strConnectionString");
            var con = new SqlConnection(conString);
            using (SqlCommand cmd = new SqlCommand("get_LoadTime"))
            {
                con.Open();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        loadTimeList.Add(Convert.ToDateTime(sdr["LOAD_TIME"]));
                    }
                }
                con.Close();
            }
            loadTimeList.OrderBy(i => i);
            List<String> datesString = loadTimeList.Select(date => date.ToString("yyyy-MM-dd")).ToList();
            return Ok(datesString);
        }
        [HttpGet]
        public IActionResult FillDataTable(string originalAssignedRorgId, string finalAssingedRorgId, string finalSupplyChainRegionId, string categoryId, string originalSupplyChainRegionId, string spandAllowanceFlagId, string interIntraRegionId, string customerId, string acceptedRejectedId, string loadTimeId, string metFlagId, string scheduledFlagId, bool originalFinalCheckId, bool originalFinalDateCheckId, bool originalFinalSKUCheckId)
        {
            List<OrderTable> orderTableList = new List<OrderTable>();
            DateTimeFormatInfo usDtfi = new CultureInfo("en-US", false).DateTimeFormat;
            string conString = configuration.GetConnectionString("strConnectionString");
            var con = new SqlConnection(conString);
            using (SqlCommand cmd = new SqlCommand("search_Orders"))
            {
                con.Open();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                if (String.IsNullOrWhiteSpace(originalAssignedRorgId))
                    cmd.Parameters.AddWithValue("@ORIGINAL_ASSIGNED_RORG", DBNull.Value);
                else
                    cmd.Parameters.AddWithValue("@ORIGINAL_ASSIGNED_RORG", originalAssignedRorgId);
                if (String.IsNullOrWhiteSpace(originalSupplyChainRegionId))
                    cmd.Parameters.AddWithValue("@ORIGINAL_SUPPLY_CHAIN_REGION", DBNull.Value);
                else
                    cmd.Parameters.AddWithValue("@ORIGINAL_SUPPLY_CHAIN_REGION", originalSupplyChainRegionId);
                if (String.IsNullOrWhiteSpace(finalAssingedRorgId))
                    cmd.Parameters.AddWithValue("@FINAL_ASSIGNED_RORG", DBNull.Value);
                else
                    cmd.Parameters.AddWithValue("@FINAL_ASSIGNED_RORG", finalAssingedRorgId);
                if (String.IsNullOrWhiteSpace(finalSupplyChainRegionId))
                    cmd.Parameters.AddWithValue("@FINAL_SUPPLY_CHAIN_REGION", DBNull.Value);
                else
                    cmd.Parameters.AddWithValue("@FINAL_SUPPLY_CHAIN_REGION", finalSupplyChainRegionId);
                if (String.IsNullOrWhiteSpace(categoryId))
                    cmd.Parameters.AddWithValue("@CATEGORY", DBNull.Value);
                else
                    cmd.Parameters.AddWithValue("@CATEGORY", categoryId);
                if (String.IsNullOrWhiteSpace(interIntraRegionId))
                    cmd.Parameters.AddWithValue("@INTER_INTRA_REGION", DBNull.Value);
                else
                    cmd.Parameters.AddWithValue("@INTER_INTRA_REGION", interIntraRegionId);
                if (String.IsNullOrWhiteSpace(customerId))
                    cmd.Parameters.AddWithValue("@CUSTOMER", DBNull.Value);
                else
                    cmd.Parameters.AddWithValue("@CUSTOMER", customerId);
                if (String.IsNullOrWhiteSpace(spandAllowanceFlagId))
                    cmd.Parameters.AddWithValue("@SPEND_ALLOWANCE_FLAG", DBNull.Value);
                else
                    cmd.Parameters.AddWithValue("@SPEND_ALLOWANCE_FLAG", spandAllowanceFlagId);
                if (String.IsNullOrWhiteSpace(acceptedRejectedId))
                    cmd.Parameters.AddWithValue("@ACCEPTEDREJECTED", DBNull.Value);
                else
                    cmd.Parameters.AddWithValue("@ACCEPTEDREJECTED", acceptedRejectedId);
                if (String.IsNullOrWhiteSpace(loadTimeId))
                    cmd.Parameters.AddWithValue("@LOAD_TIME", DBNull.Value);
                else
                {
                    //var dateList = loadTimeId.Split("/");
                    //string dateFinish = dateList[2] + "-" + dateList[0] + "-" + dateList[1];
                    cmd.Parameters.AddWithValue("@LOAD_TIME", loadTimeId);
                }
                if (String.IsNullOrWhiteSpace(metFlagId))
                    cmd.Parameters.AddWithValue("@MET_FLAG", DBNull.Value);
                else
                    cmd.Parameters.AddWithValue("@MET_FLAG", metFlagId);
                if (String.IsNullOrWhiteSpace(scheduledFlagId))
                    cmd.Parameters.AddWithValue("@SCHEDULE_FLAG ", DBNull.Value);
                else
                    cmd.Parameters.AddWithValue("@SCHEDULE_FLAG ", scheduledFlagId);
                if (originalFinalCheckId == false)
                    cmd.Parameters.AddWithValue("@Original_Final_Org", 0);
                else
                    cmd.Parameters.AddWithValue("@Original_Final_Org", 1);
                if (originalFinalDateCheckId == false)
                    cmd.Parameters.AddWithValue("@Original_Final_Date", 0);
                else
                    cmd.Parameters.AddWithValue("@Original_Final_Date", 1);
                if (originalFinalSKUCheckId == false)
                    cmd.Parameters.AddWithValue("@Original_Final_SKU", 0);
                else
                    cmd.Parameters.AddWithValue("@Original_Final_SKU", 1);
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        OrderTable orderTable = new OrderTable();
                        if (sdr["ORDERNUM"] != DBNull.Value)
                            orderTable.ORDERNUM = Convert.ToInt32(sdr["ORDERNUM"]);
                        if (sdr["ORIGINAL_ASSIGNED_SKU"] != DBNull.Value)
                            orderTable.ORIGINAL_ASSIGNED_SKU = sdr["ORIGINAL_ASSIGNED_SKU"].ToString();
                        if (sdr["ORIGINAL_ASSIGNED_RORG"] != DBNull.Value)
                            orderTable.ORIGINAL_ASSIGNED_RORG = sdr["ORIGINAL_ASSIGNED_RORG"].ToString();
                        if (sdr["ORIGINAL_SUPPLY_CHAIN_REGION"] != DBNull.Value)
                            orderTable.ORIGINAL_SUPPLY_CHAIN_REGION = sdr["ORIGINAL_SUPPLY_CHAIN_REGION"].ToString();
                        if (sdr["ORIGINAL_ASSIGNED_DATE"] != DBNull.Value)
                        {
                            var date = Convert.ToDateTime(sdr["ORIGINAL_ASSIGNED_DATE"], usDtfi);
                            //var dateList = sdr["ORIGINAL_ASSIGNED_DATE"].ToString().Split(" ");
                            //var dateListFirst = dateList[0].Split("/");
                            //string dateFinish = dateListFirst[2] + "-" + dateListFirst[0] + "-" + dateListFirst[1] + " " + dateList[1] + " " + dateList[2];
                            cmd.Parameters.AddWithValue("@ORIGINAL_ASSIGNED_DATE", date);
                            //orderTable.ORIGINAL_ASSIGNED_DATE = dateFinish;
                            orderTable.ORIGINAL_ASSIGNED_DATE = date.ToString("yyyy-MM-dd");

                        }
                        if (sdr["REQUIRED_CASES"] != DBNull.Value)
                            orderTable.REQUIRED_CASES = Convert.ToInt32(sdr["REQUIRED_CASES"]);
                        if (sdr["OPTIMAL_FLAG"] != DBNull.Value)
                            orderTable.OPTIMAL_FLAG = sdr["OPTIMAL_FLAG"].ToString();
                        if (sdr["MET_FLAG"] != DBNull.Value)
                            orderTable.MET_FLAG = sdr["MET_FLAG"].ToString();
                        if (sdr["SCHEDULE_FLAG"] != DBNull.Value)
                            orderTable.SCHEDULE_FLAG = sdr["SCHEDULE_FLAG"].ToString();
                        if (sdr["FINAL_ASSIGNED_SKU"] != DBNull.Value)
                            orderTable.FINAL_ASSIGNED_SKU = sdr["FINAL_ASSIGNED_SKU"].ToString();
                        if (sdr["FINAL_ASSIGNED_RORG"] != DBNull.Value)
                            orderTable.FINAL_ASSIGNED_RORG = sdr["FINAL_ASSIGNED_RORG"].ToString();
                        if (sdr["FINAL_SUPPLY_CHAIN_REGION"] != DBNull.Value)
                            orderTable.FINAL_SUPPLY_CHAIN_REGION = sdr["FINAL_SUPPLY_CHAIN_REGION"].ToString();
                        if (sdr["FINAL_ASSIGNED_DATE"] != DBNull.Value)
                        {
                            var date = Convert.ToDateTime(sdr["FINAL_ASSIGNED_DATE"], usDtfi);
                            //var dateList = sdr["FINAL_ASSIGNED_DATE"].ToString().Split(" ");
                            //var dateListFirst = dateList[0].Split("/");
                            //string dateFinish = dateListFirst[2] + "-" + dateListFirst[0] + "-" + dateListFirst[1] + " " + dateList[1] + " " + dateList[2];
                            cmd.Parameters.AddWithValue("@FINAL_ASSIGNED_DATE", date);
                            //orderTable.FINAL_ASSIGNED_DATE = dateFinish;
                            orderTable.FINAL_ASSIGNED_DATE = date.ToString("yyyy-MM-dd");
                        }
                        //orderTable.FINAL_ASSIGNED_DATE = Convert.ToDateTime(sdr["FINAL_ASSIGNED_DATE"]).ToShortDateString();
                        if (sdr["DELAY_IN_DAYS"] != DBNull.Value)
                            orderTable.DELAY_IN_DAYS = Convert.ToInt32(sdr["DELAY_IN_DAYS"]);
                        if (sdr["CATEGORY"] != DBNull.Value)
                            orderTable.CATEGORY = sdr["CATEGORY"].ToString();
                        if (sdr["INTER/INTRA REGION"] != DBNull.Value)
                            orderTable.INTER_INTRA_REGION = sdr["INTER/INTRA REGION"].ToString();
                        if (sdr["CUSTOMER"] != DBNull.Value)
                            orderTable.CUSTOMER = sdr["CUSTOMER"].ToString();
                        if (sdr["CUSTOMER_CITY"] != DBNull.Value)
                            orderTable.CUSTOMER_CITY = sdr["CUSTOMER_CITY"].ToString();
                        if (sdr["CUSTOMER_PO"] != DBNull.Value)
                            orderTable.CUSTOMER_PO = sdr["CUSTOMER_PO"].ToString();
                        if (sdr["DELIVERY_NUMBER"] != DBNull.Value)
                            orderTable.DELIVERY_NUMBER = sdr["DELIVERY_NUMBER"].ToString();
                        if (sdr["DTS/DC Classification"] != DBNull.Value)
                            orderTable.DTS_DC_Classification = sdr["DTS/DC Classification"].ToString();
                        if (sdr["PSN"] != DBNull.Value)
                            orderTable.PSN = Convert.ToInt32(sdr["PSN"]);
                        if (sdr["DEST_ZIP_CODE"] != DBNull.Value)
                            orderTable.DEST_ZIP_CODE = Convert.ToInt32(sdr["DEST_ZIP_CODE"]);
                        if (sdr["ONDATE"] != DBNull.Value)
                            orderTable.ONDATE = Convert.ToDouble(sdr["ONDATE"]);
                        if (sdr["OPTIMAL_ORG"] != DBNull.Value)
                            orderTable.OPTIMAL_ORG = sdr["OPTIMAL_ORG"].ToString();
                        if (sdr["NUMBER_OF_TL"] != DBNull.Value)
                            orderTable.NUMBER_OF_TL = Convert.ToDouble(sdr["NUMBER_OF_TL"]);
                        if (sdr["OPTIMAL_SPEND"] != DBNull.Value)
                            orderTable.OPTIMAL_SPEND = Convert.ToDouble(sdr["OPTIMAL_SPEND"]);
                        if (sdr["ORIGINAL_SPEND"] != DBNull.Value)
                            orderTable.ORIGINAL_SPEND = Convert.ToDouble(sdr["ORIGINAL_SPEND"]);
                        if (sdr["FINAL_TRANSPORTATION_SPEND"] != DBNull.Value)
                            orderTable.FINAL_TRANSPORTATION_SPEND = Convert.ToDouble(sdr["FINAL_TRANSPORTATION_SPEND"]);
                        if (sdr["SPEND_ALLOWANCE_PCT"] != DBNull.Value)
                            orderTable.SPEND_ALLOWANCE_PCT = Convert.ToDouble(sdr["SPEND_ALLOWANCE_PCT"]);
                        if (sdr["SPEND_ALLOWANCE_FLAG"] != DBNull.Value)
                            orderTable.SPEND_ALLOWANCE_FLAG = sdr["SPEND_ALLOWANCE_FLAG"].ToString();
                        if (sdr["TRANSPORTATION_SPEND_DIFFERENCE"] != DBNull.Value)
                            orderTable.TRANSPORTATION_SPEND_DIFFERENCE = Convert.ToDouble(sdr["TRANSPORTATION_SPEND_DIFFERENCE"]);
                        if (sdr["DYNAMIC_INCREMENTAL_SPEND"] != DBNull.Value)
                            orderTable.DYNAMIC_INCREMENTAL_SPEND = Convert.ToDouble(sdr["DYNAMIC_INCREMENTAL_SPEND"]);
                        if (sdr["DATE_TIME_CHANGED"] != DBNull.Value)
                            orderTable.DATE_TIME_CHANGED = Convert.ToDateTime(sdr["DATE_TIME_CHANGED"]).ToShortDateString();
                        if (sdr["ACCEPTED"] != DBNull.Value)
                        {
                            if (Convert.ToInt16(sdr["ACCEPTED"]) == 0)
                                orderTable.ACCEPTED = "0";
                            if (Convert.ToInt16(sdr["ACCEPTED"]) == 1)
                                orderTable.ACCEPTED = "1";
                        }
                        else
                            orderTable.ACCEPTED = "";
                        if (sdr["LOAD_TIME"] != DBNull.Value)
                        {
                            var date = Convert.ToDateTime(sdr["LOAD_TIME"], usDtfi);
                            //var dateList = sdr["LOAD_TIME"].ToString().Split(" ");
                            //var dateListFirst = dateList[0].Split("/");
                            //string dateFinish = dateListFirst[2] + "-" + dateListFirst[0] + "-" + dateListFirst[1] + " " + dateList[1] + " " + dateList[2];
                            cmd.Parameters.AddWithValue("@LOAD_TIME", date);
                            // orderTable.LOAD_TIME = dateFinish;
                            orderTable.LOAD_TIME = date.ToString("yyyy-MM-dd");
                            orderTable.LOAD_TIME_Hide = date.ToString("yyyy-MM-dd HH:mm:ss");
                            orderTable.LOAD_TIME_Show = date.ToString("MM/dd/yyyy HH:mm:ss");
                        }
                        orderTableList.Add(orderTable);
                    }
                }
                con.Close();
            }
            return Ok(orderTableList);
        }
        [HttpGet]
        public IActionResult cbChange(string origAssRorgId, string finalAssRorgId, string finalSuppChainRegionId, string categoryId, string origSuppChainregionId, string spendAllFlagId, string iiRegionId, string customerId, string metFlagId, string scheduledFlagId, string loadTimeId)
        {
            List<OrderTable> orderTableList = new List<OrderTable>();
            Dictionary<string, List<string>> cbLists = new Dictionary<string, List<string>>();
            List<string> cbIds = new List<string>();
            string conString = configuration.GetConnectionString("strConnectionString");
            var con = new SqlConnection(conString);
            using (SqlCommand cmd = new SqlCommand("search_Orders"))
            {
                con.Open();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                if (String.IsNullOrWhiteSpace(origAssRorgId))
                {
                    cmd.Parameters.AddWithValue("@ORIGINAL_ASSIGNED_RORG", DBNull.Value);
                    cbIds.Add("origAssRorgId");
                }
                else
                    cmd.Parameters.AddWithValue("@ORIGINAL_ASSIGNED_RORG", origAssRorgId);
                if (String.IsNullOrWhiteSpace(origSuppChainregionId))
                {
                    cmd.Parameters.AddWithValue("@ORIGINAL_SUPPLY_CHAIN_REGION", DBNull.Value);
                    cbIds.Add("origSuppChainregionId");
                }
                else
                    cmd.Parameters.AddWithValue("@ORIGINAL_SUPPLY_CHAIN_REGION", origSuppChainregionId);
                if (String.IsNullOrWhiteSpace(finalAssRorgId))
                {
                    cmd.Parameters.AddWithValue("@FINAL_ASSIGNED_RORG", DBNull.Value);
                    cbIds.Add("finalAssRorgId");
                }
                else
                    cmd.Parameters.AddWithValue("@FINAL_ASSIGNED_RORG", finalAssRorgId);
                if (String.IsNullOrWhiteSpace(finalSuppChainRegionId))
                {
                    cmd.Parameters.AddWithValue("@FINAL_SUPPLY_CHAIN_REGION", DBNull.Value);
                    cbIds.Add("finalSuppChainRegionId");
                }
                else
                    cmd.Parameters.AddWithValue("@FINAL_SUPPLY_CHAIN_REGION", finalSuppChainRegionId);
                if (String.IsNullOrWhiteSpace(categoryId))
                {
                    cmd.Parameters.AddWithValue("@CATEGORY", DBNull.Value);
                    cbIds.Add("categoryId");
                }
                else
                    cmd.Parameters.AddWithValue("@CATEGORY", categoryId);
                if (String.IsNullOrWhiteSpace(iiRegionId))
                {
                    cmd.Parameters.AddWithValue("@INTER_INTRA_REGION", DBNull.Value);
                    cbIds.Add("iiRegionId");
                }
                else
                    cmd.Parameters.AddWithValue("@INTER_INTRA_REGION", iiRegionId);
                if (String.IsNullOrWhiteSpace(customerId))
                {
                    cmd.Parameters.AddWithValue("@CUSTOMER", DBNull.Value);
                    cbIds.Add("customerId");
                }
                else
                    cmd.Parameters.AddWithValue("@CUSTOMER", customerId);
                if (String.IsNullOrWhiteSpace(spendAllFlagId))
                {
                    cmd.Parameters.AddWithValue("@SPEND_ALLOWANCE_FLAG", DBNull.Value);
                    cbIds.Add("spendAllFlagId");
                }
                else
                    cmd.Parameters.AddWithValue("@SPEND_ALLOWANCE_FLAG", spendAllFlagId);
                if (String.IsNullOrWhiteSpace(metFlagId))
                {
                    cmd.Parameters.AddWithValue("@MET_FLAG", DBNull.Value);
                    cbIds.Add("metFlagId");
                }
                else
                    cmd.Parameters.AddWithValue("@MET_FLAG", metFlagId);
                if (String.IsNullOrWhiteSpace(scheduledFlagId))
                {
                    cmd.Parameters.AddWithValue("@SCHEDULE_FLAG ", DBNull.Value);
                    cbIds.Add("scheduledFlagId");
                }
                else
                    cmd.Parameters.AddWithValue("@SCHEDULE_FLAG ", scheduledFlagId);
                if (String.IsNullOrWhiteSpace(loadTimeId))
                {
                    cmd.Parameters.AddWithValue("@LOAD_TIME", DBNull.Value);
                    cbIds.Add("loadTimeId");
                }
                else
                    cmd.Parameters.AddWithValue("@LOAD_TIME", loadTimeId);
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        OrderTable orderTable = new OrderTable();
                        if (sdr["ORDERNUM"] != DBNull.Value)
                            orderTable.ORDERNUM = Convert.ToInt32(sdr["ORDERNUM"]);
                        if (sdr["ORIGINAL_ASSIGNED_SKU"] != DBNull.Value)
                            orderTable.ORIGINAL_ASSIGNED_SKU = sdr["ORIGINAL_ASSIGNED_SKU"].ToString();
                        if (sdr["ORIGINAL_ASSIGNED_RORG"] != DBNull.Value)
                            orderTable.ORIGINAL_ASSIGNED_RORG = sdr["ORIGINAL_ASSIGNED_RORG"].ToString();
                        if (sdr["ORIGINAL_SUPPLY_CHAIN_REGION"] != DBNull.Value)
                            orderTable.ORIGINAL_SUPPLY_CHAIN_REGION = sdr["ORIGINAL_SUPPLY_CHAIN_REGION"].ToString();
                        if (sdr["ORIGINAL_ASSIGNED_DATE"] != DBNull.Value)
                            orderTable.ORIGINAL_ASSIGNED_DATE = Convert.ToDateTime(sdr["ORIGINAL_ASSIGNED_DATE"]).ToShortDateString();
                        if (sdr["REQUIRED_CASES"] != DBNull.Value)
                            orderTable.REQUIRED_CASES = Convert.ToInt32(sdr["REQUIRED_CASES"]);
                        if (sdr["OPTIMAL_FLAG"] != DBNull.Value)
                            orderTable.OPTIMAL_FLAG = sdr["OPTIMAL_FLAG"].ToString();
                        if (sdr["MET_FLAG"] != DBNull.Value)
                            orderTable.MET_FLAG = sdr["MET_FLAG"].ToString();
                        if (sdr["SCHEDULE_FLAG"] != DBNull.Value)
                            orderTable.SCHEDULE_FLAG = sdr["SCHEDULE_FLAG"].ToString();
                        if (sdr["FINAL_ASSIGNED_SKU"] != DBNull.Value)
                            orderTable.FINAL_ASSIGNED_SKU = sdr["FINAL_ASSIGNED_SKU"].ToString();
                        if (sdr["FINAL_ASSIGNED_RORG"] != DBNull.Value)
                            orderTable.FINAL_ASSIGNED_RORG = sdr["FINAL_ASSIGNED_RORG"].ToString();
                        if (sdr["FINAL_SUPPLY_CHAIN_REGION"] != DBNull.Value)
                            orderTable.FINAL_SUPPLY_CHAIN_REGION = sdr["FINAL_SUPPLY_CHAIN_REGION"].ToString();
                        if (sdr["FINAL_ASSIGNED_DATE"] != DBNull.Value)
                            orderTable.FINAL_ASSIGNED_DATE = Convert.ToDateTime(sdr["FINAL_ASSIGNED_DATE"]).ToShortDateString();
                        if (sdr["DELAY_IN_DAYS"] != DBNull.Value)
                            orderTable.DELAY_IN_DAYS = Convert.ToInt32(sdr["DELAY_IN_DAYS"]);
                        if (sdr["CATEGORY"] != DBNull.Value)
                            orderTable.CATEGORY = sdr["CATEGORY"].ToString();
                        if (sdr["INTER/INTRA REGION"] != DBNull.Value)
                            orderTable.INTER_INTRA_REGION = sdr["INTER/INTRA REGION"].ToString();
                        if (sdr["CUSTOMER"] != DBNull.Value)
                            orderTable.CUSTOMER = sdr["CUSTOMER"].ToString();
                        if (sdr["CUSTOMER_CITY"] != DBNull.Value)
                            orderTable.CUSTOMER_CITY = sdr["CUSTOMER_CITY"].ToString();
                        if (sdr["CUSTOMER_PO"] != DBNull.Value)
                            orderTable.CUSTOMER_PO = sdr["CUSTOMER_PO"].ToString();
                        if (sdr["DELIVERY_NUMBER"] != DBNull.Value)
                            orderTable.DELIVERY_NUMBER = sdr["DELIVERY_NUMBER"].ToString();
                        if (sdr["DTS/DC Classification"] != DBNull.Value)
                            orderTable.DTS_DC_Classification = sdr["DTS/DC Classification"].ToString();
                        if (sdr["PSN"] != DBNull.Value)
                            orderTable.PSN = Convert.ToInt32(sdr["PSN"]);
                        if (sdr["DEST_ZIP_CODE"] != DBNull.Value)
                            orderTable.DEST_ZIP_CODE = Convert.ToInt32(sdr["DEST_ZIP_CODE"]);
                        if (sdr["ONDATE"] != DBNull.Value)
                            orderTable.ONDATE = Convert.ToDouble(sdr["ONDATE"]);
                        if (sdr["OPTIMAL_ORG"] != DBNull.Value)
                            orderTable.OPTIMAL_ORG = sdr["OPTIMAL_ORG"].ToString();
                        if (sdr["NUMBER_OF_TL"] != DBNull.Value)
                            orderTable.NUMBER_OF_TL = Convert.ToDouble(sdr["NUMBER_OF_TL"]);
                        if (sdr["OPTIMAL_SPEND"] != DBNull.Value)
                            orderTable.OPTIMAL_SPEND = Convert.ToDouble(sdr["OPTIMAL_SPEND"]);
                        if (sdr["ORIGINAL_SPEND"] != DBNull.Value)
                            orderTable.ORIGINAL_SPEND = Convert.ToDouble(sdr["ORIGINAL_SPEND"]);
                        if (sdr["FINAL_TRANSPORTATION_SPEND"] != DBNull.Value)
                            orderTable.FINAL_TRANSPORTATION_SPEND = Convert.ToDouble(sdr["FINAL_TRANSPORTATION_SPEND"]);
                        if (sdr["SPEND_ALLOWANCE_PCT"] != DBNull.Value)
                            orderTable.SPEND_ALLOWANCE_PCT = Convert.ToDouble(sdr["SPEND_ALLOWANCE_PCT"]);
                        if (sdr["SPEND_ALLOWANCE_FLAG"] != DBNull.Value)
                            orderTable.SPEND_ALLOWANCE_FLAG = sdr["SPEND_ALLOWANCE_FLAG"].ToString();
                        if (sdr["TRANSPORTATION_SPEND_DIFFERENCE"] != DBNull.Value)
                            orderTable.TRANSPORTATION_SPEND_DIFFERENCE = Convert.ToDouble(sdr["TRANSPORTATION_SPEND_DIFFERENCE"]);
                        if (sdr["DYNAMIC_INCREMENTAL_SPEND"] != DBNull.Value)
                            orderTable.DYNAMIC_INCREMENTAL_SPEND = Convert.ToDouble(sdr["DYNAMIC_INCREMENTAL_SPEND"]);
                        if (sdr["DATE_TIME_CHANGED"] != DBNull.Value)
                            orderTable.DATE_TIME_CHANGED = Convert.ToDateTime(sdr["DATE_TIME_CHANGED"]).ToShortDateString();
                        if (sdr["ACCEPTED"] != DBNull.Value)
                        {
                            if (Convert.ToInt16(sdr["ACCEPTED"]) == 0)
                                orderTable.ACCEPTED = "0";
                            if (Convert.ToInt16(sdr["ACCEPTED"]) == 1)
                                orderTable.ACCEPTED = "1";
                        }
                        else
                            orderTable.ACCEPTED = null;
                        if (sdr["LOAD_TIME"] != DBNull.Value)
                            orderTable.LOAD_TIME = Convert.ToDateTime(sdr["LOAD_TIME"]).ToShortDateString();
                        orderTableList.Add(orderTable);
                    }
                }
                con.Close();
            }
            if (cbIds.Contains("origAssRorgId"))
            {
                cbLists.Add("origAssRorgId", orderTableList.Select(id => id.ORIGINAL_ASSIGNED_RORG).ToHashSet().ToList());
            }
            if (cbIds.Contains("origSuppChainregionId"))
            {
                cbLists.Add("origSuppChainregionId", orderTableList.Select(id => id.ORIGINAL_SUPPLY_CHAIN_REGION).ToHashSet().ToList());
            }
            if (cbIds.Contains("finalAssRorgId"))
            {
                cbLists.Add("finalAssRorgId", orderTableList.Select(id => id.FINAL_ASSIGNED_RORG).ToHashSet().ToList());
            }
            if (cbIds.Contains("finalSuppChainRegionId"))
            {
                cbLists.Add("finalSuppChainRegionId", orderTableList.Select(id => id.FINAL_SUPPLY_CHAIN_REGION).ToHashSet().ToList());
            }
            if (cbIds.Contains("categoryId"))
            {
                cbLists.Add("categoryId", orderTableList.Select(id => id.CATEGORY).ToHashSet().ToList());
            }
            if (cbIds.Contains("iiRegionId"))
            {
                cbLists.Add("iiRegionId", orderTableList.Select(id => id.INTER_INTRA_REGION).ToHashSet().ToList());
            }
            if (cbIds.Contains("customerId"))
            {
                cbLists.Add("customerId", orderTableList.Select(id => id.CUSTOMER).ToHashSet().ToList());
            }
            if (cbIds.Contains("spendAllFlagId"))
            {
                cbLists.Add("spendAllFlagId", orderTableList.Select(id => id.SPEND_ALLOWANCE_FLAG).ToHashSet().ToList());
            }
            if (cbIds.Contains("metFlagId"))
            {
                cbLists.Add("metFlagId", orderTableList.Select(id => id.MET_FLAG).ToHashSet().ToList());
            }
            if (cbIds.Contains("scheduledFlagId"))
            {
                cbLists.Add("scheduledFlagId", orderTableList.Select(id => id.SCHEDULE_FLAG).ToHashSet().ToList());
            }
            if (cbIds.Contains("loadTimeId"))
            {
                cbLists.Add("loadTimeId", orderTableList.Select(id => id.LOAD_TIME).ToHashSet().ToList());
            }
            return Ok(cbLists);
        }
        [HttpGet]
        public IActionResult acceptedRejected(long ordernum, string originaL_ASSIGNED_SKU, string finaL_ASSIGNED_SKU, string loaD_TIME, short accepRejectId)
        {
            string conString = configuration.GetConnectionString("strConnectionString");
            var con = new SqlConnection(conString);
            using (SqlCommand cmd = new SqlCommand("AcceptedRejected"))
            {
                con.Open();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                if (String.IsNullOrWhiteSpace(ordernum.ToString()))
                {
                    cmd.Parameters.AddWithValue("@ORDERNUM", DBNull.Value);

                }
                else
                    cmd.Parameters.AddWithValue("@ORDERNUM", ordernum);
                if (String.IsNullOrWhiteSpace(originaL_ASSIGNED_SKU))
                {
                    cmd.Parameters.AddWithValue("@ORIGINAL_ASSIGNED_SKU", DBNull.Value);
                }
                else
                    cmd.Parameters.AddWithValue("@ORIGINAL_ASSIGNED_SKU", originaL_ASSIGNED_SKU);
                if (String.IsNullOrWhiteSpace(finaL_ASSIGNED_SKU))
                {
                    cmd.Parameters.AddWithValue("@FINAL_ASSIGNED_SKU", DBNull.Value);
                }
                else
                    cmd.Parameters.AddWithValue("@FINAL_ASSIGNED_SKU", finaL_ASSIGNED_SKU);
                if (String.IsNullOrWhiteSpace(loaD_TIME))
                {
                    cmd.Parameters.AddWithValue("@LOAD_TIME", DBNull.Value);
                }
                else
                    cmd.Parameters.AddWithValue("@LOAD_TIME", loaD_TIME);
                if (String.IsNullOrWhiteSpace(accepRejectId.ToString()))
                {
                    cmd.Parameters.AddWithValue("@ACCEPTED", DBNull.Value);
                }
                else
                    cmd.Parameters.AddWithValue("@ACCEPTED", accepRejectId);
                cmd.ExecuteNonQuery();
                return Ok();
            }
        }

        public IActionResult saveSlider(string slider, string planner)
        {
            string conString = configuration.GetConnectionString("strConnectionString");
            var con = new SqlConnection(conString);
            using (SqlCommand cmd = new SqlCommand("InsertUpdateRandomAproachHP"))
            {
                con.Open();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                if (String.IsNullOrWhiteSpace(slider))
                    cmd.Parameters.AddWithValue("@HP_WEIGHT", DBNull.Value);
                else
                    cmd.Parameters.AddWithValue("@HP_WEIGHT", slider);
                if (String.IsNullOrWhiteSpace(slider.ToString()))
                    cmd.Parameters.AddWithValue("@AUTH_USER", DBNull.Value);
                else
                    cmd.Parameters.AddWithValue("@AUTH_USER", planner);
                SqlParameter insertNewRow_req = new SqlParameter("@insertNewRow", SqlDbType.Bit);
                insertNewRow_req.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(insertNewRow_req);
                SqlParameter MessageParam_req = new SqlParameter("@message", SqlDbType.NVarChar, -1);
                MessageParam_req.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(MessageParam_req);
                cmd.ExecuteNonQuery();
                int cmd_insertNewRow = Convert.ToInt32(cmd.Parameters["@insertNewRow"].Value);
                string cmd_Message = cmd.Parameters["@message"].Value.ToString();
                con.Close();
                return Ok(cmd_Message);
            }
        }

        public IActionResult GetApprach()
        {
            DateTimeFormatInfo usDtfi = new CultureInfo("en-US", false).DateTimeFormat;
            List<RandomApproachHP> randomApproachHPList = new List<RandomApproachHP>();
            string conString_approach = configuration.GetConnectionString("strConnectionString");
            var con_approach = new SqlConnection(conString_approach);
            try
            {
                using (SqlCommand cmd_approach = new SqlCommand("get_RANDOM_APPROACH_HP"))
                {
                    con_approach.Open();
                    cmd_approach.Connection = con_approach;
                    cmd_approach.CommandType = CommandType.StoredProcedure;
                    using (SqlDataReader sdr_approach = cmd_approach.ExecuteReader())
                    {
                        while (sdr_approach.Read())
                        {
                            RandomApproachHP randomApproachHP = new RandomApproachHP();
                            if (sdr_approach["INSERT_TIME"] != DBNull.Value)
                            {
                                var date = Convert.ToDateTime(sdr_approach["INSERT_TIME"], usDtfi);
                                //var dateList = sdr["ORIGINAL_ASSIGNED_DATE"].ToString().Split(" ");
                                //var dateListFirst = dateList[0].Split("/");
                                //string dateFinish = dateListFirst[2] + "-" + dateListFirst[0] + "-" + dateListFirst[1] + " " + dateList[1] + " " + dateList[2];
                                cmd_approach.Parameters.AddWithValue("@INSERT_TIME", date);
                                //orderTable.ORIGINAL_ASSIGNED_DATE = dateFinish;
                                randomApproachHP.INSERT_TIME = date.ToString("yyyy-MM-dd");

                            }

                            if (sdr_approach["HP_WEIGHT"] != DBNull.Value)
                                randomApproachHP.HP_WEIGHT = sdr_approach["HP_WEIGHT"].ToString();
                            if (sdr_approach["AUTH_USER"] != DBNull.Value)
                                randomApproachHP.AUTH_USER = sdr_approach["AUTH_USER"].ToString();

                            randomApproachHPList.Add(randomApproachHP);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                con_approach.Close();
                return Ok(ex.ToString());
            }
            finally { con_approach.Close(); }
            return Ok(randomApproachHPList);
        }
    }
}
