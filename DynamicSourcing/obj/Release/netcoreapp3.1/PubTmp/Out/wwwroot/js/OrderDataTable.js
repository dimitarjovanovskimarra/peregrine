﻿$(document).ready(function () {
    var OARList = [];
    var origSuppChainregionList = [];
    var finalAssRorgList = [];
    var finalSuppChainRegionList = [];
    var categoryList = [];
    var iiRegionList = [];
    var customerList = [];
    var spendAllFlagList = [];
    var table = $('#RANDOM_APPROACH_HP_ID').DataTable({
        "columnDefs": [{
            //targets: [-1, -3], className: 'dt-body-right'
            defaultContent: "-",
            targets: "_all"
        },
        ],

        "columns": [
            { "data": "inserT_TIME" },
            { "data": "hP_WEIGHT" },
            { "data": "autH_USER" }
        ],
    });
    var table = $('#data-table').DataTable({
        "initComplete": function (settings, json) {
            $(this).show()
        },
        "lengthMenu": [[10, 25, 50, 100, 500, 1000], [10, 25, 50, 100, 500, 1000]],
        // Design Assets
        stateSave: true,
        autoWidth: true,
        // ServerSide Setups
        processing: true,
        //serverSide: true,
        // Paging Setups
        paging: true,
        // Searching Setups
        searching: { regex: true },
        //"ajax": {
        //    "url": "/Home/FillDataTable",
        //    "type": "POST",
        //    "datatype": "json"
        //},
        "oLanguage": {
            "sLengthMenu": "Display _MENU_ records",
        },
        //fixedHeader: {
        //    header: true,
        //    footer: true
        //},
        "scrollY": "40vh",
        "scrollX": true,
        fixedColumns: {
            left: 1
        },
        //"scrollCollapse": true,
        scroller: true,
        dom: 'Bfrltip',
        select: {
            style: 'multi'
        },
        "createdRow": function (row, data, dataIndex) {
            if (data.accepted == 1) {
                $(row).css("background-color", "#00FF7F");
                //$(row).addClass("info");
            }
            if (data.accepted == 0) {
                $(row).css("background-color", "#FF6347");
                //$(row).addClass("info");
            }
            if (data.accepted == "") {
                //$(row).css("background-color", "#eeeeee");
                $(row).css("background-color", "#FAF9F9");
                //$(row).addClass("info");
            }
        },
        //"rowCallback": function (row, data, index) {

        //    if (data.accepted == 1) {
        //       // $('td', row).css('background-color', '#00FF7F');
        //        table.rows().any(function () {
        //            $(this.node()).css({ 'background-color': '#00FF7F' })
        //        })
        //    }
        //    else if (data.accepted == 0) {
        //      // $('td', row).css('background-color', '#FF6347');
        //        table.rows().any(function () {
        //            $(this.node()).css({ 'background-color': '#FF6347' })
        //        })
        //    }
        //},
        buttons: [

            {
                text: 'Select all',
                action: function () {
                    table.rows().select();
                }

            }
            ,
            {
                text: 'Select none',
                action: function () {
                    table.rows().deselect();
                }
            },
            {
                extend: 'excelHtml5',
                text: 'Save',
                title: 'Peregrine',
                exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 33]
                }
            },
        ],
        'info': false,
        //"data": "",
        responsive: {
            details: {
                renderer: function (api, rowIdx, columns) {
                    var data = $.map(columns, function (col, i) {
                        return col.hidden ?
                            '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                            '<td>' + col.title + ':' + '</td> ' +
                            '<td>' + col.data + '</td>' +
                            '</tr>' :
                            '';
                    }).join('');
                    return data ?
                        $('<table/>').append(data) :
                        false;
                }
            }
        },
        "columnDefs": [{
            //targets: [-1, -3], className: 'dt-body-right'
            defaultContent: "-",
            targets: "_all"
        },
        ],

        "columns": [
            { "data": "ordernum" },
            { "data": "originaL_ASSIGNED_SKU" },
            { "data": "originaL_ASSIGNED_RORG" },
            { "data": "originaL_SUPPLY_CHAIN_REGION" },
            { "data": "originaL_ASSIGNED_DATE" },
            { "data": "requireD_CASES" },
            { "data": "optimaL_FLAG" },
            { "data": "meT_FLAG" },
            { "data": "schedulE_FLAG" },
            { "data": "finaL_ASSIGNED_SKU" },
            { "data": "finaL_ASSIGNED_RORG" },
            { "data": "finaL_SUPPLY_CHAIN_REGION" },
            { "data": "finaL_ASSIGNED_DATE" },
            { "data": "delaY_IN_DAYS" },
            { "data": "category" },
            { "data": "inteR_INTRA_REGION" },
            { "data": "customer" },
            { "data": "customer_city" },
            { "data": "customer_state" },
            { "data": "dtS_DC_Classification" },
            { "data": "psn" },
            { "data": "desT_ZIP_CODE" },
            { "data": "ondate" },
            { "data": "optimaL_ORG" },
            { "data": "numbeR_OF_TL" },
            { "data": "optimaL_SPEND" },
            { "data": "originaL_SPEND" },
            { "data": "finaL_TRANSPORTATION_SPEND" },
            { "data": "spenD_ALLOWANCE_PCT" },
            { "data": "spenD_ALLOWANCE_FLAG" },
            { "data": "transportatioN_SPEND_DIFFERENCE" },
            { "data": "dynamiC_INCREMENTAL_SPEND" },
            { "data": "datE_TIME_CHANGED" },
            { "data": "accepted" },
            { "data": "loaD_TIME" },
            { "data": "loaD_TIME_Show" }
        ],
    });
    table.columns([33]).visible(false);
    // new $.fn.dataTable.FixedHeader(table);
    $("tbody tr").on("click", function () {
        //loop through all td elements in the row
        $(this).find("td").each(function (i) {
            //toggle between adding/removing the 'active' class
            $(this).toggleClass('active');
        });
    });


     
        
        $.ajax({
            url: "/Home/Category",
            type: "GET",
            success: function (result) {
                categoryList = result;
                for (var i = 0; i < categoryList.sort().length; i++) {
                    $('#categoryId').append('<option>' + categoryList[i] + '</option>');
                }
            }
        }),
     

       
      
      
        $.ajax({
            url: "/Home/LoadTime",
            type: "GET",
            success: function (result) {
                for (var i = 0; i < result.length; i++) {
                    $('#loadTimeId').append('<option>' + result[i] + '</option>');
                }

                $.ajax({
                    url: "/Home/Customer",
                    type: "GET",
                    data: {
                        loadTimeId: $("#loadTimeId option:selected").text()
                    },
                    success: function (result) {
                        customerList = result;
                        for (var i = 0; i < customerList.sort().length; i++) {
                            $('#customerId').append('<option>' + customerList[i] + '</option>');
                        }
                    }
                }),
                    $.ajax({
                        url: "/Home/FillOriginalAssignedRorg",
                        type: "GET",
                        data: {
                            loadTimeId: $("#loadTimeId option:selected").text()
                        },
                        success: function (result) {
                            OARList = result;
                            for (var i = 0; i < OARList.sort().length; i++) {
                                $('#origAssRorgId').append('<option>' + OARList[i] + '</option>');
                            }
                        }
                    }),
                    $.ajax({
                        url: "/Home/FillFinalAssingedRorg",
                        type: "GET",
                        data: {
                            loadTimeId: $("#loadTimeId option:selected").text()
                        },
                        success: function (result) {
                            finalAssRorgList = result;
                            for (var i = 0; i < finalAssRorgList.sort().length; i++) {
                                $('#finalAssRorgId').append('<option>' + finalAssRorgList[i] + '</option>');
                            }
                        }
                    }),
                    $.ajax({
                        url: "/Home/InterIntraRegion",
                        type: "GET",
                        data: {
                            loadTimeId: $("#loadTimeId option:selected").text()
                        },
                        success: function (result) {
                            iiRegionList = result;
                            for (var i = 0; i < iiRegionList.sort().length; i++) {
                                $('#iiRegionId').append('<option>' + iiRegionList[i] + '</option>');
                            }
                        }
                    }),
                    $.ajax({
                        url: "/Home/FillOriginalSupplyChainRegion",
                        type: "GET",
                        data: {
                            loadTimeId: $("#loadTimeId option:selected").text()
                        },
                        success: function (result) {
                            origSuppChainregionList = result;
                            for (var i = 0; i < origSuppChainregionList.sort().length; i++) {
                                $('#origSuppChainregionId').append('<option>' + origSuppChainregionList[i] + '</option>');
                            }
                        }
                    }),
                    $.ajax({
                        url: "/Home/FinalSupplyChainRegion",
                        type: "GET",
                        data: {
                            loadTimeId: $("#loadTimeId option:selected").text()
                        },
                        success: function (result) {
                            finalSuppChainRegionList = result;
                            for (var i = 0; i < finalSuppChainRegionList.sort().length; i++) {
                                $('#finalSuppChainRegionId').append('<option>' + finalSuppChainRegionList[i] + '</option>');
                            }
                        }
                    }),
                    $.ajax({
                        url: "/Home/MetFlag",
                        type: "GET",
                        data: {
                            loadTimeId: $("#loadTimeId option:selected").text()
                        },
                        success: function (result) {
                            for (var i = 0; i < result.length; i++) {
                                $('#metFlagId').append('<option>' + result[i] + '</option>');
                            }
                        }
                    }),
                    $.ajax({
                        url: "/Home/ScheduleFlag",
                        type: "GET",
                        data: {
                            loadTimeId: $("#loadTimeId option:selected").text()
                        },
                        success: function (result) {
                            for (var i = 0; i < result.length; i++) {
                                $('#scheduledFlagId').append('<option>' + result[i] + '</option>');
                            }
                        }
                    }),
                    $.ajax({
                        url: "/Home/SpandAllowanceFlag",
                        type: "GET",
                        data: {
                            loadTimeId: $("#loadTimeId option:selected").text()
                        },
                        success: function (result) {
                            spendAllFlagList = result;
                            for (var i = 0; i < spendAllFlagList.sort().length; i++) {
                                $('#spendAllFlagId').append('<option>' + spendAllFlagList[i] + '</option>');
                            }
                        }
                    })
            }
        }),
        $("#searchBtnId").click(function () {
            search();
        })
    $("#acceptBtnId").click(function () {
        var tableAccept = $('#data-table').DataTable();
        for (var i = 0; i < table.rows('.selected').data().length; i++) {
            // tableAccept.cell({ row: tableAccept.rows('.selected').indexes()[i], column: 31 }).data('1');
            tableAccept.rows({ selected: true }).every(function () {
                $(this.node()).css({ 'background-color': '#00FF7F' })
                // var cell = tableAccept.cell({ row: tableAccept.rows('.selected').indexes()[i], column: 31 }).node();
                //  $(cell).css({ 'background-color': '#00FF7F' });
            })
            $.ajax({
                url: "/Home/acceptedRejected",
                type: "GET",
                data: {
                    ordernum: tableAccept.rows('.selected').data()[i].ordernum,
                    originaL_ASSIGNED_SKU: tableAccept.rows('.selected').data()[i].originaL_ASSIGNED_SKU,
                    finaL_ASSIGNED_SKU: tableAccept.rows('.selected').data()[i].finaL_ASSIGNED_SKU,
                    loaD_TIME: tableAccept.rows('.selected').data()[i].loaD_TIME_Hide,
                    accepRejectId: 1
                },
                success: function (result) {


                }
            })
        }
        tableAccept.rows().deselect();
    })
    $("#rejectBtnId").click(function () {
        var tableReject = $('#data-table').DataTable();
        for (var i = 0; i < tableReject.rows('.selected').data().length; i++) {
            // tableReject.cell({ row: tableReject.rows('.selected').indexes()[i], column: 31 }).data('0');
            tableReject.rows({ selected: true }).every(function () {
                $(this.node()).css({ 'background-color': '#FF6347' })
                //  var cell = tableReject.cell({ row: tableReject.rows('.selected').indexes()[i], column: 31 }).node();
                // $(cell).css({ 'background-color': '#FF6347' });
            })
            $.ajax({
                url: "/Home/acceptedRejected",
                type: "GET",
                data: {
                    ordernum: tableReject.rows('.selected').data()[i].ordernum,
                    originaL_ASSIGNED_SKU: tableReject.rows('.selected').data()[i].originaL_ASSIGNED_SKU,
                    finaL_ASSIGNED_SKU: tableReject.rows('.selected').data()[i].finaL_ASSIGNED_SKU,
                    loaD_TIME: tableReject.rows('.selected').data()[i].loaD_TIME_Hide,
                    accepRejectId: 0
                },
                success: function (result) {

                }
            })
        }
        tableReject.rows().deselect();
    })
    $("#origAssRorgId").change(function () {
        fillCB();
    })
    $("#origSuppChainregionId").change(function () {
        fillCB();
    })
    $("#finalAssRorgId").change(function () {
        fillCB();
    })
    $("#finalSuppChainRegionId").change(function () {
        fillCB();
    })
    $("#categoryId").change(function () {
        fillCB();
    })
    $("#iiRegionId").change(function () {
        fillCB();
    })
    $("#customerId").change(function () {
        fillCB();
    })
    $("#spendAllFlagId").change(function () {
        fillCB();
    })
    $("#metFlagId").change(function () {
        fillCB();
    })
    $("#scheduledFlagId").change(function () {
        fillCB();
    })
    $("#loadTimeId").change(function () {
        fillCB();
    })
    function fillCB() {
        $.ajax({
            url: "/Home/cbChange",
            type: "GET",
            data: {
                origAssRorgId: $("#origAssRorgId option:selected").text(),
                finalAssRorgId: $("#finalAssRorgId option:selected").text(),
                finalSuppChainRegionId: $("#finalSuppChainRegionId option:selected").text(),
                categoryId: $("#categoryId option:selected").text(),
                origSuppChainregionId: $("#origSuppChainregionId option:selected").text(),
                spendAllFlagId: $("#spendAllFlagId option:selected").text(),
                iiRegionId: $("#iiRegionId option:selected").text(),
                customerId: $("#customerId option:selected").text(),
                metFlagId: $("#metFlagId option:selected").text(),
                scheduledFlagId: $("#scheduledFlagId option:selected").text(),
                loadTimeId: $("#loadTimeId option:selected").text()
            },
            success: function (result) {
                if ("origAssRorgId" in result) {
                    $("#origAssRorgId").empty();
                    $('#origAssRorgId').append('<option></option>');
                    for (var i = 0; i < result["origAssRorgId"].sort().length; i++) {
                        $('#origAssRorgId').append('<option>' + result["origAssRorgId"][i] + '</option>');
                    }
                }
                if ("categoryId" in result) {
                    $("#categoryId").empty();
                    $('#categoryId').append('<option></option>');
                    for (var i = 0; i < result["categoryId"].sort().length; i++) {
                        $('#categoryId').append('<option>' + result["categoryId"][i] + '</option>');
                    }
                }
                if ("customerId" in result) {
                    $("#customerId").empty();
                    $('#customerId').append('<option></option>');
                    for (var i = 0; i < result["customerId"].sort().length; i++) {
                        $('#customerId').append('<option>' + result["customerId"][i] + '</option>');
                    }
                }
                if ("finalAssRorgId" in result) {
                    $("#finalAssRorgId").empty();
                    $('#finalAssRorgId').append('<option></option>');
                    for (var i = 0; i < result["finalAssRorgId"].sort().length; i++) {
                        $('#finalAssRorgId').append('<option>' + result["finalAssRorgId"][i] + '</option>');
                    }
                }
                if ("finalSuppChainRegionId" in result) {
                    $("#finalSuppChainRegionId").empty();
                    $('#finalSuppChainRegionId').append('<option></option>');
                    for (var i = 0; i < result["finalSuppChainRegionId"].sort().length; i++) {
                        $('#finalSuppChainRegionId').append('<option>' + result["finalSuppChainRegionId"][i] + '</option>');
                    }
                }
                if ("iiRegionId" in result) {
                    $("#iiRegionId").empty();
                    $('#iiRegionId').append('<option></option>');
                    for (var i = 0; i < result["iiRegionId"].sort().length; i++) {
                        $('#iiRegionId').append('<option>' + result["iiRegionId"][i] + '</option>');
                    }
                }
                if ("origSuppChainregionId" in result) {
                    $("#origSuppChainregionId").empty();
                    $('#origSuppChainregionId').append('<option></option>');
                    for (var i = 0; i < result["origSuppChainregionId"].sort().length; i++) {
                        $('#origSuppChainregionId').append('<option>' + result["origSuppChainregionId"][i] + '</option>');
                    }
                }
                if ("spendAllFlagId" in result) {
                    $("#spendAllFlagId").empty();
                    $('#spendAllFlagId').append('<option></option>');
                    for (var i = 0; i < result["spendAllFlagId"].sort().length; i++) {
                        $('#spendAllFlagId').append('<option>' + result["spendAllFlagId"][i] + '</option>');
                    }
                }
                if ("metFlagId" in result) {
                    $("#metFlagId").empty();
                    $('#metFlagId').append('<option></option>');
                    for (var i = 0; i < result["metFlagId"].sort().length; i++) {
                        $('#metFlagId').append('<option>' + result["metFlagId"][i] + '</option>');
                    }
                }
                if ("scheduledFlagId" in result) {
                    $("#scheduledFlagId").empty();
                    $('#scheduledFlagId').append('<option></option>');
                    for (var i = 0; i < result["scheduledFlagId"].sort().length; i++) {
                        $('#scheduledFlagId').append('<option>' + result["scheduledFlagId"][i] + '</option>');
                    }
                }
                if ("loadTimeId" in result) {
                    $("#loadTimeId").empty();
                    $('#loadTimeId').append('<option></option>');
                    for (var i = 0; i < result["loadTimeId"].sort().length; i++) {
                        $('#loadTimeId').append('<option>' + result["loadTimeId"][i] + '</option>');
                    }
                }
            }
        })
    }
    function search() {
        $('#data-table').dataTable().fnClearTable();
        $.ajax({
            url: "/Home/FillDataTable",
            type: "GET",
            data: {
                originalAssignedRorgId: $("#origAssRorgId option:selected").text(),
                finalAssingedRorgId: $("#finalAssRorgId option:selected").text(),
                finalSupplyChainRegionId: $("#finalSuppChainRegionId option:selected").text(),
                categoryId: $("#categoryId option:selected").text(),
                originalSupplyChainRegionId: $("#origSuppChainregionId option:selected").text(),
                spandAllowanceFlagId: $("#spendAllFlagId option:selected").text(),
                interIntraRegionId: $("#iiRegionId option:selected").text(),
                customerId: $("#customerId option:selected").text(),
                acceptedRejectedId: $("#acceptedRejectedId option:selected").text(),
                loadTimeId: $("#loadTimeId option:selected").text(),
                metFlagId: $("#metFlagId option:selected").text(),
                scheduledFlagId: $("#scheduledFlagId option:selected").text(),
                originalFinalCheckId: $("#originalFinalCheckId").is(":checked"),
                originalFinalDateCheckId: $("#originalFinalDateCheckId").is(":checked"),
                originalFinalSKUCheckId: $("#originalFinalSKUCheckId").is(":checked"),
            },
            beforeSend: function () {
                $('#preloadId').show()
            },
            success: function (result) {
                $('#data-table').dataTable().fnAddData(result);
                $('#preloadId').hide();
            }
        })
    }
    $("#saveId").click(function () {
        $.ajax({
            url: "/Home/saveSlider",
            data: {
                slider: document.getElementById('customRangeId').value,
                planner: "Demo"
            },
            success: function (result) {
                alert(result);
                randomApproach();
            },
            error: function (result) {
                alert(result)
            }
        })
    })
    $("#btnRandomApproachId").click(function () {
        randomApproach()
    })
    function randomApproach() {
        $('#RANDOM_APPROACH_HP_ID').dataTable().fnClearTable();
        $.ajax({
            url: "/Home/GetApprach",
            type: "GET",
            success: function (result) {
                $('#RANDOM_APPROACH_HP_ID').dataTable().fnAddData(result);
            },
            error: function (result) {
                alert(result)
            }
        })
    }
})