﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DynamicSourcing.Models
{
    public class OrderTable
    {
        public virtual int ORDERNUM { get; set; }
        public virtual string ORIGINAL_ASSIGNED_SKU { get; set; }
        public virtual string ORIGINAL_ASSIGNED_RORG { get; set; }
        public virtual string ORIGINAL_SUPPLY_CHAIN_REGION { get; set; }
        public virtual string ORIGINAL_ASSIGNED_DATE { get; set; }
        public virtual int REQUIRED_CASES { get; set; }
        public virtual string OPTIMAL_FLAG { get; set; }
        public virtual string MET_FLAG { get; set; }
        public virtual string SCHEDULE_FLAG { get; set; }
        public virtual string FINAL_ASSIGNED_SKU { get; set; }
        public virtual string FINAL_ASSIGNED_RORG { get; set; }
        public virtual string FINAL_SUPPLY_CHAIN_REGION { get; set; }
        public virtual string FINAL_ASSIGNED_DATE { get; set; }
        public virtual int DELAY_IN_DAYS { get; set; }
        public virtual string CATEGORY { get; set; }
        public virtual string INTER_INTRA_REGION { get; set; }
        public virtual string CUSTOMER { get; set; }
        public virtual string CUSTOMER_CITY { get; set; }
        public virtual string CUSTOMER_PO { get; set; }
        public virtual string DELIVERY_NUMBER { get; set; }
        public virtual string DTS_DC_Classification { get; set; }
        public virtual int PSN { get; set; }
        public virtual int DEST_ZIP_CODE { get; set; }
        public virtual double ONDATE { get; set; }
        public virtual string OPTIMAL_ORG { get; set; }
        public virtual double NUMBER_OF_TL { get; set; }
        public virtual double OPTIMAL_SPEND { get; set; }
        public virtual double ORIGINAL_SPEND { get; set; }
        public virtual double FINAL_TRANSPORTATION_SPEND { get; set; }
        public virtual double SPEND_ALLOWANCE_PCT { get; set; }
        public virtual string SPEND_ALLOWANCE_FLAG { get; set; }
        public virtual double TRANSPORTATION_SPEND_DIFFERENCE { get; set; }
        public virtual double DYNAMIC_INCREMENTAL_SPEND { get; set; }
        public virtual string DATE_TIME_CHANGED { get; set; }
        public virtual String ACCEPTED { get; set; }
        public virtual string LOAD_TIME { get; set; }
        public virtual string LOAD_TIME_Hide { get; set; }
        public virtual string LOAD_TIME_Show { get; set; }
    }
}
